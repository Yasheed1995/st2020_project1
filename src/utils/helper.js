
let sorting = array => {
    array.sort((a, b) => {return a - b})
    return array;
}

let compare = (a, b) => {
    if (a['PM2.5'] > b['PM2.5']) {
        return 1
    }
    else if (a['PM2.5'] < b['PM2.5']) {
        return -1 
    }
    return 0
}

let average = nums => {
    const arrSum = arr => arr.reduce((a, b) => a + b, 0)
    return Math.round(arrSum(nums) / nums.length * 100) / 100
}


module.exports = {
    sorting,
    compare,
    average
}
